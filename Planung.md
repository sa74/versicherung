# <Projekt> Planungsübersicht

| Bezeichnung    | Angaben        |
| -------------- | ---------------|
| Autor:       |Paramanantharajah|
| Erstellt am:   |       24.03.2023         |
| Git-Repo-URL   | [Repo](https://gitlab.com/sa74/versicherung/-/tree/main/versicherungsrechner) |
|                Vorlage | P. Rutschmann                |

---

# Planung vom 24.03.2023




## Liste der noch anstehenden Ideen

* Grundstruktur der React App
* Dokumentation Stand ausformulieren



## Liste der Arbeiten, die ich am erledigen bin

* Zeitraum: 24.03.23

#### offen

* Dokumentation Stand ausformulieren


#### in Arbeit

* Grundstruktur der React App


#### erledigt



## Liste der zuvor abgeschlossenen Arbeiten

* Planung
* Keyframes
* React App aufgesetzt 

---

# Planung vom 31.03.2023




## Liste der noch anstehenden Ideen

* Feature Ideen Mindmap
* Planung verbessern



## Liste der Arbeiten, die ich am erledigen bin

* Zeitraum: 31.03.23 - 07.04.23

#### offen

* Dokumentation nachbessern


#### in Arbeit

* Feature Ideen Mindmap
* Planung verbessern


#### erledigt

* Grundstruktur der React App

## Liste der zuvor abgeschlossenen Arbeiten

* Planung
* Keyframes
* React App aufgesetzt 
* Grundstruktur der React App

---

# Planung vom 07.04.2023




## Liste der noch anstehenden Ideen

* Calculator Component erstellen
* Design der App verbessern



## Liste der Arbeiten, die ich am erledigen bin

* Zeitraum: 07.04.23 - 14.04.23

#### offen

* Design der App verbessern



#### in Arbeit

* Calculator Component erstellen


#### erledigt

* Feature Ideen Mindmap
* Planung verbessern


## Liste der zuvor abgeschlossenen Arbeiten

* Planung
* Keyframes
* React App aufgesetzt 
* Grundstruktur der React App
* Feature Ideen Mindmap
* Planung verbessern
* Calculator Component erstellen

---

# Planung vom 14.04.2023




## Liste der noch anstehenden Ideen

* Design der App verbessern
* Navbar erstellen



## Liste der Arbeiten, die ich am erledigen bin

* Zeitraum: 14.04.23 - 21.04.23

#### offen

* Design der App verbessern
* Navbar erstellen


#### in Arbeit

* Navbar erstellen


#### erledigt



## Liste der zuvor abgeschlossenen Arbeiten

* Planung
* Keyframes
* React App aufgesetzt 
* Grundstruktur der React App
* Feature Ideen Mindmap
* Planung verbessern
* Calculator Component erstellen

---

# Planung vom 05.05.2023




## Liste der noch anstehenden Ideen

* Design der App verbessern
* Quadratmoddel Variante erstellen
* About Seite erstellen
* Dokumentation ausformulieren
* Readme.md überarbeiten
* Dockerfile erstellen



## Liste der Arbeiten, die ich am erledigen bin

* Zeitraum: 05.05.23 - 12.05.23

#### offen

* Dokumentation ausformulieren


#### in Arbeit

* Dockerfile erstellen
* Readme.md überarbeiten


#### erledigt

* Design der App verbessern
* Quadratmoddel Variante erstellen
* About Seite erstellen 



## Liste der zuvor abgeschlossenen Arbeiten

* Planung
* Keyframes
* React App aufgesetzt 
* Grundstruktur der React App
* Feature Ideen Mindmap
* Planung verbessern
* Calculator Component erstellen
* Navbar erstellen
* Design der App verbessern
* Quadratmoddel Variante erstellen
* About Seite erstellen 