# Versicherungsrechner

This calculator was made for a school project. It calculates the insurance coverage for houses and apartments. The user can choose between different types of insurance.
It was made by Sananjayan Paramanantharajah during a time period of 3 months. There was work done in- and outside of school. The supervisor was Mr. Rutschmann.
Personally this Project was a great experience for me. I learned a lot about React and how to use it. 
The calculator works just fine but is not quite finished yet. There are still some features missing. I will continue to work on it in my free time.


## Getting Started

If you want to try out the calculator you can download the code and run it on your local machine.

### Prerequisites

You need some things to run the calculator.
* npm
  ```sh
  npm install npm@latest -g
  ```

### Installation

_Below is an example of how you can instruct your audience on installing and setting up your app. This template doesn't rely on any external dependencies or services._

1. Clone the repo
   
        git clone https://gitlab.com/sa74/versicherung

2. Move into directory
   
        cd versicherungsrechner
   
3. Install NPM packages
    
        npm install

4. Open in vscode
    
        code .
    
5. Start the App
        
            npm start

## Usage

Now you can use the calculator. You can choose between two different types of insurance. 
Or you can add own features to the calculator.
Be sure to have fun with it.

## Contact

- Project Link: [https://gitlab.com/sa74/versicherung](https://gitlab.com/sa74/versicherung)
- Email: sananjayan.paramanantharajah@lernende.bbw.ch 