import logo from './logo.svg';
import './App.css';
import Calculator from './components/Calculator';
import Startpage from './pages/Startpage';
import { BrowserRouter, Routes, Route } from "react-router-dom"
import { Button } from './components/button';
import { Navbar } from './components/Navbar';
import  CalculatorQuadrat  from './components/CalculatorQuadrat';
import Aboutpage from './pages/Aboutpage';
import { ChakraProvider } from '@chakra-ui/react'



function App() {
  return (

    
      <div>
        <BrowserRouter>
          <Navbar />
            <Routes>
              <Route path="/" element={<Startpage />} />
              <Route path="calculator" element={<Calculator />} />
              <Route path="CalculatorQuadrat" element={<CalculatorQuadrat />} />
              <Route path="startpage" element={<Startpage />} />
              <Route path="Aboutpage" element={<Aboutpage />} />
              <Route path="*" element={<h1>404: Not Found</h1>} />
            </Routes>
        </BrowserRouter>  
      </div>
    
    
  );
}

export default App;
