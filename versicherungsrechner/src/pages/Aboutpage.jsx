import Calculator from "../components/Calculator";
import { Routes } from 'react-router-dom';
import { Route } from 'react-router-dom';

export function Aboutpage(){
    

    return(
        
        <div id="centering">
            <br></br>
            <br />
            <br />
            <br />
            <p>Herzlich willkommen zum Versicherungsrechner. Dieser Rechner wurde von Sananjayan Paramanantharajah erstellt und wird kontinuirlich weiter entwickelt. Sananjayan ist ein Informatik Schüler in der IMS, welcher die Kantonsschule Büelrain besucht. </p>
            <p>Der Versicherungsrechner ist ein Projekt, welches im Rahmen des Informatikunterrichts entstanden ist. </p>
            <br />
            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2695.718381856231!2d8.727989376849376!3d47.49539929564925!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x479a999e9fc4c32d%3A0xc5f240ca8689ae56!2sKantonsschule%20B%C3%BCelrain%20Winterthur%2C%20Wirtschaftsgymnasium%2C%20Handels-%20und%20Informatikmittelschule!5e0!3m2!1sde!2sch!4v1683836454804!5m2!1sde!2sch" width="600" height="450"  allowfullscreen="" loading="lazy" referrerpolicy="no-referrer-when-downgrade">
            </iframe>
            <br />
            <p>sananjayan.paramanantharajah@stud.kbw.ch</p>


    
        </div>

    )
}
export default Aboutpage;