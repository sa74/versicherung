import Calculator from "../components/Calculator";
import { Routes, useNavigate } from 'react-router-dom';
import { Route } from 'react-router-dom';

export function Startpage(){
    const navigate = useNavigate();

    function handleClick(){
        navigate("/calculator")
    }

    return(
        <div>
            <div>
                <h1 id="hoverColor">verSANerung</h1>
            </div>
            <div>
                <button id="startbutton" onClick={handleClick}>Start</button>
            </div>
            
                
        </div>

    )
}
export default Startpage;