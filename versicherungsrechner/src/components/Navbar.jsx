import React from 'react';
import { Link } from 'react-router-dom';

export function Navbar () {
  return (
    <ul>
      <li><Link to="/Startpage">Home</Link></li>
      <li><Link to="/Calculator">Privatkunden</Link></li>
      <li><Link to="/CalculatorQuadrat">Unternehmenskunden</Link></li>
      <li><Link to="/Aboutpage">About us</Link></li>
    </ul>
  );
}
