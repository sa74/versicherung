import React, { useState } from 'react'
function Calculator() {


    
        const [houseValue, setHouseValue] = useState(0)
        const [insuranceValue, setInsuranceValue] = useState(0)
        const [damage, setDamage] = useState(0)
        const [result, setResult] = useState("")
         
        const calculate = () => {
            if (houseValue === 0){
                return
            } 
            const tempresult = insuranceValue / houseValue * damage 
            if (tempresult > insuranceValue){
                setResult("Sie sind unterversichert!")
            } else {
                console.log(tempresult)
                setResult("Ihr Schadensersatz beträgt: " + tempresult)
            }
        }

        
    
    return (
        <div>
            <h2>Willkommen zum Versicherungsrechner</h2>
            <p>Geben Sie Ihre Daten ein:</p>
            <br></br>
            <div id='box'>
                <form onSubmit={(e) => {e.preventDefault()}}>
                    <label>Gesamtwert des Hauses:
                        <input value={houseValue} onChange={(e) => setHouseValue(e.target.value)}  type="text" />
                    </label>
                    <br></br>
                    <label>Versicherungswert:
                        <input value={insuranceValue} onChange={(e) => setInsuranceValue(e.target.value)} type="text" />
                    </label>
                    <br></br>
                    <label>Schaden:
                        <input value={damage} onChange={(e) => setDamage(e.target.value)} type="text" />
                    </label>
                    <br></br>
                
                    <input className="click" type="submit" value="Berechnen" onClick={calculate}/>
                    <br></br>
                    <p>{result}</p>
                </form>
            </div>
        </div>
    )
}

export default Calculator