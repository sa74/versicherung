import React, { useState } from 'react'
function CalculatorQuadrat() {


    
        const [area, setArea] = useState(0)
        const [areaValue, setAreaValue] = useState(0)
        const [damage, setDamage] = useState(0)
        const [result, setResult] = useState("")
         
        const calculate = () => {
            if (areaValue === 0){
                return
            } 
            const tempresult = area / areaValue * damage 
            if (tempresult > area * areaValue){
                setResult("Sie sind unterversichert!")
            } else {
                console.log(tempresult)
                setResult("Ihr Schadensersatz beträgt: " + tempresult)
            }
        }

        
    
    return (
        <div>
            <h2>Willkommen zum Versicherungsrechner</h2>
            
            <h3>Dies ist die Quadratmeter-Methode für unsere Unternehmenskunden</h3>
            <p>Geben Sie Ihre Daten ein:</p>
            <br></br>
            <div id='box'>
                <form onSubmit={(e) => {e.preventDefault()}}>
                    <label>Anzahl der Quadratmeter:
                        <input value={area} onChange={(e) => setArea(e.target.value)}  type="text" />
                    </label>
                    <br></br>
                    <label>Wert pro Quadratmeter:
                        <input value={areaValue} onChange={(e) => setAreaValue(e.target.value)} type="text" />
                    </label>
                    <br></br>
                    <label>Schaden:
                        <input value={damage} onChange={(e) => setDamage(e.target.value)} type="text" />
                    </label>
                    <br></br>
                
                    <input className="click" type="submit" value="Berechnen" onClick={calculate}/>
                    <br />
                    <p>{result}</p>
                </form>
            </div>
        </div>
    )
}

export default CalculatorQuadrat