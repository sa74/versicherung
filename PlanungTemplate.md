# <Projekt> Planungsübersicht

| Bezeichnung    | Angaben        |
| -------------- | ---------------|
| Autor:       |Paramanantharajah|
| Erstellt am:   |       24.03.2023         |
| Git-Repo-URL   | [Repo](https://gitlab.com/sa74/versicherung/-/tree/main/versicherungsrechner) |
|                Vorlage | P. Rutschmann                |

---

# Planung vom 24.03.2023


---

## Liste der noch anstehenden Ideen

* Grundstruktur der React App
* Dokumentation Stand ausformulieren

---

## Liste der Arbeiten, die ich am erledigen bin

* Zeitraum: 24.03.23

#### offen

* Dokumentation Stand ausformulieren


#### in Arbeit

* Grundstruktur der React App


#### erledigt



---

## Liste der zuvor abgeschlossenen Arbeiten

* Planung
* Keyframes
* React App aufgesetzt 
